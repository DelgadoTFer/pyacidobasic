DESTDIR = 
LRELEASE = lrelease -qt5
PYPACKAGE = pyacidobasic
SHAREDIR = $(DESTDIR)/usr/share/$(PYPACKAGE)
SOURCES = $(shell ls pymecavideo/*.py | grep -v ^Ui_)
UI_SOURCES = $(shell ls pymecavideo/*.ui)
UI_TARGETS = $(patsubst %.ui, Ui_%.py, $(UI_SOURCES))

all:	pyacidobasic.1.gz languages
	make -C pyacidobasic all

pyacidobasic.1.gz: pyacidobasic.1
	gzip -c9 $< > $@

pyacidobasic.1: manpage.xml
	xsltproc --nonet /usr/share/sgml/docbook/stylesheet/xsl/nwalsh/manpages/docbook.xsl manpage.xml

clean:
	make -C pyacidobasic clean
	rm -f *~ *.pyc *.1 *.1.gz lang/*.qm

install:
	install -d $(SHAREDIR)
	cp -a Pyacidobasic pyacidobasic lang *.acb $(SHAREDIR)
	find $(DESTDIR) -type d -name __pycache__ | xargs rm -rf
	mkdir -p $(DESTDIR)/usr/share/applications
	cp pyacidobasic.desktop $(DESTDIR)/usr/share/applications
	install -d $(DESTDIR)/usr/bin
	install -m 755 pyacidobasic.sh $(DESTDIR)/usr/bin/pyacidobasic
	install -d $(DESTDIR)/usr/share/man/man1
	install -m 644 pyacidobasic.1.gz $(DESTDIR)/usr/share/man/man1
	install -d $(DESTDIR)/usr/share/pixmaps
	install -m 644 pyacidobasic/icons/pyacidobasic.svg $(DESTDIR)/usr/share/pixmaps


languages: tsfiles
	$(LRELEASE) lang/*.ts

tsfiles: 
	cd lang; pylupdate5 pyacidobasic.pro
	# fix inconsistencies of pylupdate5 which cannot honor the
	# directive CODEC in pyacidobasic.pro
	#for f in lang/*.ts; do \
	#  utils/fix-lupdate5.py $$f $$f.tmp && mv $$f.tmp $$f; \
	#done

helpfiles:
	@$(MAKE) -C help

.PHONY: clean all install languages helpfiles tsfiles
