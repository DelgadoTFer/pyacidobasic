#!/usr/bin/python3
### setup.py ###

from distutils.core import setup
import glob
import pyacidobasic.version

package='pyacidobasic'
command='Pyacidobasic'
manpage='pyacidobasic.1.gz' 

setup (name=package,
      version=pyacidobasic.version.version,
      description=u"%s permet de simuler des titrages acido-basiques" %package,
      author='Georges KHAZNADAR',
      author_email='georgesk@debian.org',
      url='http://outilsphysiques.tuxfamily.org/pmwiki.php/Oppl/Pyacidobasic',
      license='GPLv3',
      packages=[package],
      package_dir={package: 'pyacidobasic'},
      data_files=[
          ('share/%s/lang' %package, glob.glob("lang/*.html")+ glob.glob('lang/*.ts')+ glob.glob('lang/*.qm')),
          ('share/%s' %package, glob.glob("*.acb")),
          ('bin', [command]),
          ('share/man/man1', [manpage])
      ]
)
