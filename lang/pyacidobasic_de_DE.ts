<?xml version="1.0" ?><!DOCTYPE TS><TS language="de_DE" sourcelanguage="fr_FR" version="2.0">
<context>
    <name>Dialog</name>
    <message utf8="true">
        <location filename="../prelevement.ui" line="14"/>
        <source>Define a sample</source>
        <translation>Bestimmen Sie die Probe</translation>
    </message>
    <message utf8="true">
        <location filename="../prelevement.ui" line="45"/>
        <source>Sample: </source>
        <translation>Probe:</translation>
    </message>
    <message>
        <location filename="../prelevement.ui" line="61"/>
        <source>Concentration (mol/L)</source>
        <translation>Konzentration (mol/L)</translation>
    </message>
    <message>
        <location filename="../prelevement.ui" line="68"/>
        <source>0.1</source>
        <translation>0.1</translation>
    </message>
    <message>
        <location filename="../prelevement.ui" line="75"/>
        <source>Volume (mL)</source>
        <translation>Volumen (mL)</translation>
    </message>
    <message>
        <location filename="../prelevement.ui" line="82"/>
        <source>10.0</source>
        <translation>10.0</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../curvecontrol.ui" line="14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../curvecontrol.ui" line="50"/>
        <source>Click to show/hide the plot</source>
        <translation>Hier klicken, um den Graphen anzuzeigen bzw. ihn zu verbergen</translation>
    </message>
    <message>
        <location filename="../curvecontrol.ui" line="106"/>
        <source>Click to change the color of the plot</source>
        <translation>Hier klicken, um die Farbe es Graphen zu ändern</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../main.ui" line="71"/>
        <source>Laboratory</source>
        <translation>Labor</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="101"/>
        <source>Choose reagents</source>
        <translation>Wahl der Reagenzien</translation>
    </message>
    <message>
        <location filename="../main.ui" line="186"/>
        <source>Burette</source>
        <translation>Bürette</translation>
    </message>
    <message>
        <location filename="../main.ui" line="664"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="246"/>
        <source>Beaker</source>
        <translation>Becherglas</translation>
    </message>
    <message>
        <location filename="../main.ui" line="724"/>
        <source>toolBar</source>
        <translation>Werkzeugleiste</translation>
    </message>
    <message>
        <location filename="../main.ui" line="787"/>
        <source>pH</source>
        <translation>pH</translation>
    </message>
    <message>
        <location filename="../main.ui" line="26"/>
        <source>pyAcidoBasic</source>
        <translation>pyAcidoBasic</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="152"/>
        <source>Choose a reagent and drag it to the burette or the beaker</source>
        <translation>Wählen Sie eine Reagenz und ziehen Sie diese in die Bürette oder das Becherglas</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="155"/>
        <source>List of reagents: drag a reagent to the burette or the beaker.</source>
        <translation>Liste der Reagenzien: Ziehen eine Reagens in die Bürette oder den Becher.</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="198"/>
        <source>drain the burette to fill it again</source>
        <translation>Entleeren Sie die Bürette, um sie wieder zu füllen</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="227"/>
        <source>drag a reagent to the burette</source>
        <translation>Ziehen Sie eine Reagenz auf die Bürette</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="230"/>
        <source>Reagent of the burette: drag a reagent from the above list.</source>
        <translation>Reagenzien der Bürette: Ziehen Sie eine Reagenz aus der obigen Liste.</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="255"/>
        <source>drain the beaker to fill it again</source>
        <translation>Leeren Sie das Becherglas, um es erneut zu füllen</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="284"/>
        <source>drag one or more reagents to the beaker</source>
        <translation>Ziehen Sie eine oder mehrere Reagenzien in das Becherglas</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="287"/>
        <source>List of reagents of the beaker: drag the reagents from the above list.</source>
        <translation>Reagenzien des Becherglases: Ziehen Sie eine/mehrere Reagenzien aus der obigen Liste.</translation>
    </message>
    <message>
        <location filename="../main.ui" line="579"/>
        <source>export in PDF format</source>
        <translation>Exportieren als PDF</translation>
    </message>
    <message>
        <location filename="../main.ui" line="596"/>
        <source>export in JPEG format</source>
        <translation>Exportieren als JPEG</translation>
    </message>
    <message>
        <location filename="../main.ui" line="613"/>
        <source>export in SVG format</source>
        <translation>Exportieren im SVG Format</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="630"/>
        <source>Define the title</source>
        <translation>Wählen Sie den Namen</translation>
    </message>
    <message>
        <location filename="../main.ui" line="644"/>
        <source>make the abscissa range wider</source>
        <translation>Vergrößerung der Abszissenachse</translation>
    </message>
    <message>
        <location filename="../main.ui" line="661"/>
        <source>make the abscissa range narrower</source>
        <translation>Verkleinerung der Abszissenachse </translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="471"/>
        <source>Curves to plot</source>
        <translation>Kurve zeichnen</translation>
    </message>
    <message>
        <location filename="../main.ui" line="697"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../main.ui" line="707"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../main.ui" line="736"/>
        <source>&amp;Open ... (Ctrl-O)</source>
        <translation>&amp;Öffnen ... (Strg-O)</translation>
    </message>
    <message>
        <location filename="../main.ui" line="744"/>
        <source>&amp;Save ... (Ctrl-S)</source>
        <translation>&amp;Speichern ... (Strg-S)</translation>
    </message>
    <message>
        <location filename="../main.ui" line="752"/>
        <source>Enregistrer Sous ... (Maj Ctrl -S)</source>
        <translation>Speichern als ... (Shift-Strg-S)</translation>
    </message>
    <message>
        <location filename="../main.ui" line="757"/>
        <source>&amp;Quit (Ctrl-Q)</source>
        <translation>&amp;Beenden (Strg-Q)</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="765"/>
        <source>&amp;About...</source>
        <translation>&amp;Über...</translation>
    </message>
    <message>
        <location filename="../main.ui" line="770"/>
        <source>&amp;Manual (F1)</source>
        <translation>&amp;Anleitung (F1)</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="795"/>
        <source>dérivée</source>
        <translation>Derivate</translation>
    </message>
    <message>
        <location filename="../main.ui" line="803"/>
        <source>[H3O+]</source>
        <translation>[H3O+]</translation>
    </message>
    <message>
        <location filename="../main.ui" line="811"/>
        <source>[HO-]</source>
        <translation>[HO-]</translation>
    </message>
    <message>
        <location filename="../main.ui" line="816"/>
        <source>Others... (Ctrl-T)</source>
        <translation>Andere ... (Strg-T)</translation>
    </message>
    <message>
        <location filename="../main.ui" line="824"/>
        <source>Exemples ...</source>
        <translation>Beispiele ...</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="122"/>
        <source>Filtre :</source>
        <translation>Filter:</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="110"/>
        <source>Type in a few charcters of the reagent</source>
        <translation>Geben Sie einige Buchstaben der Reagenz ein</translation>
    </message>
    <message>
        <location filename="../main.ui" line="135"/>
        <source>Click to delete the filter</source>
        <translation>Klicken um den Filter zurückzusetzen</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="451"/>
        <source>Concentrations/Quantities</source>
        <translation>Konzentrationen/Mengen</translation>
    </message>
    <message>
        <location filename="../main.ui" line="498"/>
        <source>Concentrations</source>
        <translation>Konzentrationen</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="508"/>
        <source>Quantités</source>
        <translation>Mengen</translation>
    </message>
    <message utf8="true">
        <location filename="../main.ui" line="486"/>
        <source>Choose the type of plot</source>
        <translation>Wählen Sie die Art des Graphen</translation>
    </message>
</context>
<context>
    <name>pyacidobasic</name>
    <message>
        <location filename="../__init__.py" line="296"/>
        <source>File to save</source>
        <translation>Datei zum Speichern</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="296"/>
        <source>Acidobasic files [*.acb] (*.acb);; All files (*.* *)</source>
        <translation>Acidobasic Dateitypen [*.acb] (*.acb);; Alle Dateien (*.* *)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="308"/>
        <source>Version error</source>
        <translation>Fehler in dieser Version</translation>
    </message>
    <message utf8="true">
        <location filename="../__init__.py" line="320"/>
        <source>About</source>
        <translation>Über Acidobasic</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="380"/>
        <source>Courbe du dosage</source>
        <translation>Titrations Graph</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="308"/>
        <source>Ce fichier n'est pas un fichier Pyacidobasic valide, de version %1.</source>
        <translation>Ungültige oder beschädigte Datei! (Version %1).</translation>
    </message>
</context>
</TS>