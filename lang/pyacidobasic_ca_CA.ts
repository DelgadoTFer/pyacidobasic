<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="ca_ES" sourcelanguage="fr_FR">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="14"/>
        <source>Define a sample</source>
        <translation type="unfinished">Definició d&apos;un assaig</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="45"/>
        <source>Sample: </source>
        <translation type="unfinished">Assaig:</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="61"/>
        <source>Concentration (mol/L)</source>
        <translation type="unfinished">Concentració (mol/L)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="68"/>
        <source>0.1</source>
        <translation type="unfinished">0.1</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="75"/>
        <source>Volume (mL)</source>
        <translation type="unfinished">Volum (mL)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="82"/>
        <source>10.0</source>
        <translation type="unfinished">10.0</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Form</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="50"/>
        <source>Click to show/hide the plot</source>
        <translation type="unfinished">Clic per mostrar/ocultar la corba</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="106"/>
        <source>Click to change the color of the plot</source>
        <translation type="unfinished">clic per canviar el color de la corba</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../pyacidobasic/main.ui" line="74"/>
        <source>Laboratory</source>
        <translation type="unfinished">Laboratori</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="104"/>
        <source>Choose reagents</source>
        <translation type="unfinished">Elecció dels reactius</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="189"/>
        <source>Burette</source>
        <translation type="unfinished">Bureta</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="728"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="249"/>
        <source>Beaker</source>
        <translation type="unfinished">Vas de precipitats</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="788"/>
        <source>toolBar</source>
        <translation type="unfinished">toolbar</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="851"/>
        <source>pH</source>
        <translation type="unfinished">pH</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="26"/>
        <source>pyAcidoBasic</source>
        <translation type="unfinished">pyacidoBasic</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="155"/>
        <source>Choose a reagent and drag it to the burette or the beaker</source>
        <translation type="unfinished">Triar un reactiu i arrastrar-lo cap a la burera o el vas</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="158"/>
        <source>List of reagents: drag a reagent to the burette or the beaker.</source>
        <translation type="unfinished">Llista de reactius: Arrossegar un reactiu cap a la bureta o el vas de precipitats</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="201"/>
        <source>drain the burette to fill it again</source>
        <translation type="unfinished">buidar la bureta per omplir-la de nou</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="230"/>
        <source>drag a reagent to the burette</source>
        <translation type="unfinished">arrossegar un reactiu cap a la bureta</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="233"/>
        <source>Reagent of the burette: drag a reagent from the above list.</source>
        <translation type="unfinished">Reactiu de la bureta: arrossegar un reactiu des de la llista de dalt</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="258"/>
        <source>drain the beaker to fill it again</source>
        <translation type="unfinished">buidar el vas per omplir-lo de nou</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="287"/>
        <source>drag one or more reagents to the beaker</source>
        <translation type="unfinished">arrossegar un o més reactius al vas</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="290"/>
        <source>List of reagents of the beaker: drag the reagents from the above list.</source>
        <translation type="unfinished">Llista de reactius del vas: arrossegar els reactius des de la llista superior.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="643"/>
        <source>export in PDF format</source>
        <translation type="unfinished">exportar a format pdf</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="660"/>
        <source>export in JPEG format</source>
        <translation type="unfinished">exportar a format jpeg</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="677"/>
        <source>export in SVG format</source>
        <translation type="unfinished">exportar a format svg</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="694"/>
        <source>Define the title</source>
        <translation type="unfinished">Definir la concentració</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="708"/>
        <source>make the abscissa range wider</source>
        <translation type="unfinished">augmentar l&apos;interval de les abscisses</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="725"/>
        <source>make the abscissa range narrower</source>
        <translation type="unfinished">disminuir l&apos;interval de les abscisses</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="535"/>
        <source>Curves to plot</source>
        <translation type="unfinished">corbes a traçar</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="761"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Fitxer</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="771"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Ajuda</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="800"/>
        <source>&amp;Open ... (Ctrl-O)</source>
        <translation type="unfinished">&amp;Obrir ... (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="808"/>
        <source>&amp;Save ... (Ctrl-S)</source>
        <translation type="unfinished">Guardar ... (Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../main.ui" line="752"/>
        <source>Enregistrer Sous ... (Maj Ctrl -S)</source>
        <translation type="obsolete">Guardar com ... (Maj Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="821"/>
        <source>&amp;Quit (Ctrl-Q)</source>
        <translation type="unfinished">Cancel·lar (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="829"/>
        <source>&amp;About ...</source>
        <translation type="unfinished">Sobre ...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="834"/>
        <source>&amp;Manual (F1)</source>
        <translation type="unfinished">&amp;Manual (F1)</translation>
    </message>
    <message>
        <location filename="../main.ui" line="850"/>
        <source>d&#xe9;riv&#xe9;e</source>
        <translation type="obsolete">derivada</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="867"/>
        <source>[H3O+]</source>
        <translation type="unfinished">[H3O+]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="875"/>
        <source>[HO-]</source>
        <translation type="unfinished">[HO-]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="880"/>
        <source>Others... (Ctrl-T)</source>
        <translation type="unfinished">Altres ...(Ctrl-T)</translation>
    </message>
    <message>
        <location filename="../main.ui" line="824"/>
        <source>Exemples ...</source>
        <translation type="obsolete">Exemples ...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="125"/>
        <source>Filter:</source>
        <translation type="unfinished">Filtre :</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="113"/>
        <source>Type in a few charcters of the reagent</source>
        <translation type="unfinished">Drecera: Teclejar algunes lletras del reactiu</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="138"/>
        <source>Click to delete the filter</source>
        <translation type="unfinished">Clicar per eliminar el filtre</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="515"/>
        <source>Concentrations/Quantities</source>
        <translation type="unfinished">Concentracions/Quantitats</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="562"/>
        <source>&amp;Concentrations</source>
        <translation type="unfinished">Concentracions</translation>
    </message>
    <message>
        <location filename="../main.ui" line="508"/>
        <source>Quantit&#xe9;s</source>
        <translation type="obsolete">Quantitats</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="550"/>
        <source>Choose the type of plot</source>
        <translation type="unfinished">Triar el tipus de representació</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="572"/>
        <source>&amp;Quantities</source>
        <translation type="unfinished">Quantitats</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="816"/>
        <source>Save &amp;as ... (Shift Ctrl -S)</source>
        <translation type="unfinished">Guardar com ... (Maj Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="888"/>
        <source>&amp;Examples ...</source>
        <translation type="unfinished">Exemples ...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="110"/>
        <source>Type in a few characters of the reagent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="322"/>
        <source>Color indicator ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="327"/>
        <source>Bromothymol blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="332"/>
        <source>Phenolphtalein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="337"/>
        <source>Helianthin (Methyl orange)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="342"/>
        <source>Bromophenol blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="347"/>
        <source>Methyl red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="859"/>
        <source>derivative</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pyacidobasic</name>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="270"/>
        <source>File to save</source>
        <translation type="unfinished">Fitxer a guardar</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="270"/>
        <source>Acidobasic files [*.acb] (*.acb);; All files (*.* *)</source>
        <translation type="unfinished">Fitxers Acidobasic [*.acb] (*.acb);; Qualsevol fitxer (*.* *)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="282"/>
        <source>Version error</source>
        <translation type="unfinished">Error de versió</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="294"/>
        <source>About</source>
        <translation type="unfinished">Sobre</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="371"/>
        <source>Courbe du dosage</source>
        <translation type="obsolete">Corba de valoració</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="299"/>
        <source>Ce fichier n&apos;est pas un fichier Pyacidobasic valide, de version %1.</source>
        <translation type="obsolete">Aquest fitxer no es un fitxer Pyacidobasic vàlid, versió %1.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="70"/>
        <source>Sorry, missing support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="70"/>
        <source>The current version of pyqtgraph does not yet support PDF exports. Try to export SVG then convert it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="94"/>
        <source>Message about the bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="94"/>
        <source>Error: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="87"/>
        <source>A bug is possible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="87"/>
        <source>A bug is possible, the developer has patched the file ImageExport of the package pyqtgraph, and filed a bug report. As long as it is not fixed, there will be an issue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="282"/>
        <source>This file is not a valid Pyacidobasic file, version %s.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
