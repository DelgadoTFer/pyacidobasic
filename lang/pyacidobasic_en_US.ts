<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="en_US" sourcelanguage="fr_FR">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="61"/>
        <source>Concentration (mol/L)</source>
        <translation type="unfinished">Concentration (mol/L)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="68"/>
        <source>0.1</source>
        <translation type="unfinished">0.1</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="75"/>
        <source>Volume (mL)</source>
        <translation type="unfinished">Volume (mL)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="82"/>
        <source>10.0</source>
        <translation type="unfinished">10.0</translation>
    </message>
    <message>
        <location filename="../prelevement.ui" line="14"/>
        <source>D&#xe9;finition d&apos;un pr&#xe9;l&#xe8;vement</source>
        <translation type="obsolete">Define a sample</translation>
    </message>
    <message>
        <location filename="../prelevement.ui" line="45"/>
        <source>Pr&#xe9;l&#xe8;vement : </source>
        <translation type="obsolete">Sample: </translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="14"/>
        <source>Define a sample</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="45"/>
        <source>Sample: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Form</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="50"/>
        <source>Click to show/hide the plot</source>
        <translation type="unfinished">Click to show/hide the plot</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="106"/>
        <source>Click to change the color of the plot</source>
        <translation type="unfinished">Click to change the color of the plot</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../pyacidobasic/main.ui" line="74"/>
        <source>Laboratory</source>
        <translation type="unfinished">Laboratory</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="189"/>
        <source>Burette</source>
        <translation type="unfinished">Burette</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="728"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="788"/>
        <source>toolBar</source>
        <translation type="unfinished">toolBar</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="851"/>
        <source>pH</source>
        <translation type="unfinished">pH</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="26"/>
        <source>pyAcidoBasic</source>
        <translation type="unfinished">pyAcidoBasic</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="643"/>
        <source>export in PDF format</source>
        <translation type="unfinished">export in PDF format</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="660"/>
        <source>export in JPEG format</source>
        <translation type="unfinished">export in JPEG format</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="677"/>
        <source>export in SVG format</source>
        <translation type="unfinished">export in SVG format</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="708"/>
        <source>make the abscissa range wider</source>
        <translation type="unfinished">make the abscissa range wider</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="725"/>
        <source>make the abscissa range narrower</source>
        <translation type="unfinished">make the abscissa range narrower</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="562"/>
        <source>&amp;Concentrations</source>
        <translation type="unfinished">&amp;Concentrations</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="761"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;File</translation>
    </message>
    <message>
        <location filename="../main.ui" line="762"/>
        <source>&amp;Aide</source>
        <translation type="obsolete">&amp;Help</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="800"/>
        <source>&amp;Open ... (Ctrl-O)</source>
        <translation type="unfinished">&amp;Open ... (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="808"/>
        <source>&amp;Save ... (Ctrl-S)</source>
        <translation type="unfinished">&amp;Save ... (Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="816"/>
        <source>Save &amp;as ... (Shift Ctrl -S)</source>
        <translation type="unfinished">Save &amp;as ... (Shift-Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="821"/>
        <source>&amp;Quit (Ctrl-Q)</source>
        <translation type="unfinished">&amp;Quit (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="834"/>
        <source>&amp;Manual (F1)</source>
        <translation type="unfinished">&amp;Manual (F1)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="867"/>
        <source>[H3O+]</source>
        <translation type="unfinished">[H3O+]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="875"/>
        <source>[HO-]</source>
        <translation type="unfinished">[HO-]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="880"/>
        <source>Others... (Ctrl-T)</source>
        <translation type="unfinished">Others... (Ctrl-T)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="888"/>
        <source>&amp;Examples ...</source>
        <translation type="unfinished">&amp;Examples...</translation>
    </message>
    <message>
        <location filename="../main.ui" line="101"/>
        <source>Choix des r&#xe9;actifs</source>
        <translation type="obsolete">Choose reagents</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="155"/>
        <source>Choose a reagent and drag it to the burette or the beaker</source>
        <translation type="unfinished">Choose a reagent and drag it to the burette or the beaker</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="158"/>
        <source>List of reagents: drag a reagent to the burette or the beaker.</source>
        <translation type="unfinished">List of reagents: drag a reagent to the burette or the beaker.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="201"/>
        <source>drain the burette to fill it again</source>
        <translation type="unfinished">drain the burette to fill it again</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="230"/>
        <source>drag a reagent to the burette</source>
        <translation type="unfinished">drag a reagent to the burette</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="233"/>
        <source>Reagent of the burette: drag a reagent from the above list.</source>
        <translation type="unfinished">Reagent of the burette: drag a reagent from the above list.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="249"/>
        <source>Beaker</source>
        <translation type="unfinished">Beaker</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="258"/>
        <source>drain the beaker to fill it again</source>
        <translation type="unfinished">drain the beaker to fill it again</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="287"/>
        <source>drag one or more reagents to the beaker</source>
        <translation type="unfinished">drag one or more reagents to the beaker</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="290"/>
        <source>List of reagents of the beaker: drag the reagents from the above list.</source>
        <translation type="unfinished">List of reagents of the beaker: drag the reagents from the above list.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="694"/>
        <source>Define the title</source>
        <translation type="unfinished">Define the title</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="535"/>
        <source>Curves to plot</source>
        <translation type="unfinished">Curves to plot</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="829"/>
        <source>&amp;About ...</source>
        <translation type="unfinished">&amp;About...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="125"/>
        <source>Filter:</source>
        <translation type="unfinished">Filter:</translation>
    </message>
    <message>
        <location filename="../main.ui" line="110"/>
        <source>Taper quelques lettres du r&#xe9;actif pour le s&#xe9;lectionner plus facilement</source>
        <translation type="obsolete">Type in a few charcters of the reagent</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="138"/>
        <source>Click to delete the filter</source>
        <translation type="unfinished">Click to delete the filter</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="515"/>
        <source>Concentrations/Quantities</source>
        <translation type="unfinished">Concentrations/Quantities</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="572"/>
        <source>&amp;Quantities</source>
        <translation type="unfinished">&amp;Quantities</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="550"/>
        <source>Choose the type of plot</source>
        <translation type="unfinished">Choose the type of plot</translation>
    </message>
    <message>
        <location filename="../main.ui" line="324"/>
        <source>BBT</source>
        <translation type="obsolete">Bromothymol blue</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="332"/>
        <source>Phenolphtalein</source>
        <translation type="unfinished">Phenolphtalein</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="337"/>
        <source>Helianthin (Methyl orange)</source>
        <translation type="unfinished">Helianthin (Methyl orange)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="342"/>
        <source>Bromophenol blue</source>
        <translation type="unfinished">Bromophenol blue</translation>
    </message>
    <message>
        <location filename="../main.ui" line="344"/>
        <source>Rouge de m&#xe9;thyle</source>
        <translation type="obsolete">Methyl red</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="322"/>
        <source>Color indicator ...</source>
        <translation type="unfinished">Color indicator ...</translation>
    </message>
    <message>
        <location filename="../main.ui" line="850"/>
        <source>d&#xe9;riv&#xe9;e</source>
        <translation type="obsolete">derivative</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="104"/>
        <source>Choose reagents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="110"/>
        <source>Type in a few characters of the reagent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="113"/>
        <source>Type in a few charcters of the reagent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="327"/>
        <source>Bromothymol blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="347"/>
        <source>Methyl red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="771"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="859"/>
        <source>derivative</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pyacidobasic</name>
    <message>
        <location filename="../mainwindow.py" line="252"/>
        <source>Fichier pour enregistrer</source>
        <translation type="obsolete">File to save</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="264"/>
        <source>Erreur de version</source>
        <translation type="obsolete">Version error</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="646"/>
        <source>Courbe du dosage</source>
        <translation type="obsolete">Titration plot</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="354"/>
        <source>Ce fichier n&apos;est pas un fichier Pyacidobasic valide, de version %1.</source>
        <translation type="obsolete">This file is not a valid Pyacidobasic file (version %1).</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="70"/>
        <source>Sorry, missing support</source>
        <translation type="unfinished">Sorry, missing support</translation>
    </message>
    <message>
        <location filename="../phplot.py" line="70"/>
        <source>La version courante de pyqtgraph ne supporte pas encore l&apos;export en PDF. Essayez de passer par SVG puis un convertisseur</source>
        <translation type="obsolete">The current version of pyqtgraph does not yet support PDF exports. Try to export SVG then convert it</translation>
    </message>
    <message>
        <location filename="../phplot.py" line="94"/>
        <source>Message relatif au bug</source>
        <translation type="obsolete">Message about the bug</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="94"/>
        <source>Error: %s</source>
        <translation type="unfinished">Error: %s</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="87"/>
        <source>A bug is possible</source>
        <translation type="unfinished">A bug is possible</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="282"/>
        <source>This file is not a valid Pyacidobasic file, version %s.</source>
        <translation type="unfinished">This file is not a valid Pyacidobasic file, version %s.</translation>
    </message>
    <message>
        <location filename="../phplot.py" line="87"/>
        <source>Un bug est possible, le d&#xe9;veloppeur a patch&#xe9; le fichier ImageExport du paquet pyqtgraph, et post&#xe9; un rapport de bug. Tant que ce rapport n&apos;est pas trait&#xe9;, il y aura des soucis.</source>
        <translation type="obsolete">A bug is possible, the developer has patched the file ImageExport in the package pyqtgraph, and posted a bug report. As long as this bug is not fixed, there will be an issue.</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="276"/>
        <source>&#xc0; propos</source>
        <translation type="obsolete">About</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="70"/>
        <source>The current version of pyqtgraph does not yet support PDF exports. Try to export SVG then convert it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="94"/>
        <source>Message about the bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="87"/>
        <source>A bug is possible, the developer has patched the file ImageExport of the package pyqtgraph, and filed a bug report. As long as it is not fixed, there will be an issue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="270"/>
        <source>File to save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="270"/>
        <source>Acidobasic files [*.acb] (*.acb);; All files (*.* *)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="282"/>
        <source>Version error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="294"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
