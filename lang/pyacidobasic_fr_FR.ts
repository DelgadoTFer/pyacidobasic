<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="14"/>
        <source>Define a sample</source>
        <translation type="unfinished">Définition d&apos;un prélèvement</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="45"/>
        <source>Sample: </source>
        <translation type="unfinished">Prélèvement : </translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="61"/>
        <source>Concentration (mol/L)</source>
        <translation type="unfinished">Concentration (mol/L)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="68"/>
        <source>0.1</source>
        <translation type="unfinished">0.1</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="75"/>
        <source>Volume (mL)</source>
        <translation type="unfinished">Volume (mL)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="82"/>
        <source>10.0</source>
        <translation type="unfinished">10.0</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Form</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="50"/>
        <source>Click to show/hide the plot</source>
        <translation type="unfinished">Cliquer pour afficher/cacher la courbe</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="106"/>
        <source>Click to change the color of the plot</source>
        <translation type="unfinished">cliquer pour changer la couleur de la courbe</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../pyacidobasic/main.ui" line="74"/>
        <source>Laboratory</source>
        <translation type="unfinished">Laboratoire</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="104"/>
        <source>Choose reagents</source>
        <translation type="unfinished">Choix des réactifs</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="189"/>
        <source>Burette</source>
        <translation type="unfinished">Burette</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="728"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="249"/>
        <source>Beaker</source>
        <translation type="unfinished">Bécher</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="788"/>
        <source>toolBar</source>
        <translation type="unfinished">barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="851"/>
        <source>pH</source>
        <translation type="unfinished">pH</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="26"/>
        <source>pyAcidoBasic</source>
        <translation type="unfinished">pyAcidoBasic</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="155"/>
        <source>Choose a reagent and drag it to the burette or the beaker</source>
        <translation type="unfinished">Choisr un réactif et le tirer vers la burette ou le bécher</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="158"/>
        <source>List of reagents: drag a reagent to the burette or the beaker.</source>
        <translation type="unfinished">Liste des réactifs : Tirer un réactif vers la burette ou vers le bécher.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="201"/>
        <source>drain the burette to fill it again</source>
        <translation type="unfinished">vider la burette pour la remplir à nouveau</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="230"/>
        <source>drag a reagent to the burette</source>
        <translation type="unfinished">tirer un réactif vers la burette</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="233"/>
        <source>Reagent of the burette: drag a reagent from the above list.</source>
        <translation type="unfinished">Réactif de la burette : tirer un réactif depuis la liste ci-dessus.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="258"/>
        <source>drain the beaker to fill it again</source>
        <translation type="unfinished">vider le bécher pour la remplir à nouveau</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="287"/>
        <source>drag one or more reagents to the beaker</source>
        <translation type="unfinished">tirer un ou plusieurs réactifs vers le bécher</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="290"/>
        <source>List of reagents of the beaker: drag the reagents from the above list.</source>
        <translation type="unfinished">Liste des réactifs du bécher : tirer des réactifs de la liste ci-dessus.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="643"/>
        <source>export in PDF format</source>
        <translation type="unfinished">exporter au format PDF</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="660"/>
        <source>export in JPEG format</source>
        <translation type="unfinished">exporter au format JPEG</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="677"/>
        <source>export in SVG format</source>
        <translation type="unfinished">exporter au format SVG</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="694"/>
        <source>Define the title</source>
        <translation type="unfinished">Définir le titre</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="708"/>
        <source>make the abscissa range wider</source>
        <translation type="unfinished">augmenter l&apos;intervalle des abscisses</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="725"/>
        <source>make the abscissa range narrower</source>
        <translation type="unfinished">diminuer l&apos;intervalle des abscisses</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="535"/>
        <source>Curves to plot</source>
        <translation type="unfinished">Courbes à tracer</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="761"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="771"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Aide</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="800"/>
        <source>&amp;Open ... (Ctrl-O)</source>
        <translation type="unfinished">&amp;Ouvrir ... (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="808"/>
        <source>&amp;Save ... (Ctrl-S)</source>
        <translation type="unfinished">Enregi&amp;strer ... (Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="816"/>
        <source>Save &amp;as ... (Shift Ctrl -S)</source>
        <translation type="unfinished">&amp;Enregistrer Sous ... (Maj Ctrl -S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="821"/>
        <source>&amp;Quit (Ctrl-Q)</source>
        <translation type="unfinished">&amp;Quitter (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="829"/>
        <source>&amp;About ...</source>
        <translation type="unfinished">À &amp;propos ...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="834"/>
        <source>&amp;Manual (F1)</source>
        <translation type="unfinished">&amp;Manuel (F1)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="859"/>
        <source>derivative</source>
        <translation type="unfinished">dérivée</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="867"/>
        <source>[H3O+]</source>
        <translation type="unfinished">[H3O+]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="875"/>
        <source>[HO-]</source>
        <translation type="unfinished">[HO-]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="880"/>
        <source>Others... (Ctrl-T)</source>
        <translation type="unfinished">Autres ... (Ctrl-T)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="888"/>
        <source>&amp;Examples ...</source>
        <translation type="unfinished">&amp;Exemples ...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="125"/>
        <source>Filter:</source>
        <translation type="unfinished">Filtre :</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="113"/>
        <source>Type in a few charcters of the reagent</source>
        <translation type="unfinished">Taper quelques lettres du réactif pour le sélectionner plus facilement</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="138"/>
        <source>Click to delete the filter</source>
        <translation type="unfinished">Cliquer pour effacer le filtre</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="515"/>
        <source>Concentrations/Quantities</source>
        <translation type="unfinished">Concentrations/Quantités</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="562"/>
        <source>&amp;Concentrations</source>
        <translation type="unfinished">&amp;Concentrations</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="572"/>
        <source>&amp;Quantities</source>
        <translation type="unfinished">&amp;Quantités</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="550"/>
        <source>Choose the type of plot</source>
        <translation type="unfinished">Choisir le type de représentation</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="327"/>
        <source>Bromothymol blue</source>
        <translation type="unfinished">BBT</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="332"/>
        <source>Phenolphtalein</source>
        <translation type="unfinished">Phénolphtaléine</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="337"/>
        <source>Helianthin (Methyl orange)</source>
        <translation type="unfinished">Hélianthine</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="342"/>
        <source>Bromophenol blue</source>
        <translation type="unfinished">Bleu de bromophénol</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="347"/>
        <source>Methyl red</source>
        <translation type="unfinished">Rouge de méthyle</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="322"/>
        <source>Color indicator ...</source>
        <translation type="unfinished">Indicateur coloré ...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="110"/>
        <source>Type in a few characters of the reagent</source>
        <translation type="unfinished">Taper quelques caractères du réactif</translation>
    </message>
</context>
<context>
    <name>pyacidobasic</name>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="70"/>
        <source>Sorry, missing support</source>
        <translation type="unfinished">Désolé, support incomplet</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="70"/>
        <source>The current version of pyqtgraph does not yet support PDF exports. Try to export SVG then convert it</source>
        <translation type="unfinished">La version courante de pyqtgraph ne supporte pas encore l&apos;export en PDF. Essayez de passer par SVG puis un convertisseur</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="94"/>
        <source>Message about the bug</source>
        <translation type="unfinished">Message relatif au bug</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="94"/>
        <source>Error: %s</source>
        <translation type="unfinished">Erreur : %s</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="87"/>
        <source>A bug is possible</source>
        <translation type="unfinished">Un bug est possible</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="270"/>
        <source>File to save</source>
        <translation type="unfinished">Fichier pour enregistrer</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="270"/>
        <source>Acidobasic files [*.acb] (*.acb);; All files (*.* *)</source>
        <translation type="unfinished">Fichiers Acidobasic [*.acb] (*.acb);; Tous types de fichiers (*.* *)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="282"/>
        <source>Version error</source>
        <translation type="unfinished">Erreur de version</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="282"/>
        <source>This file is not a valid Pyacidobasic file, version %s.</source>
        <translation type="unfinished">Ce fichier n&apos;est pas un fichier Pyacidobasic valide, de version %s.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="294"/>
        <source>About</source>
        <translation type="unfinished">À propos</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="87"/>
        <source>A bug is possible, the developer has patched the file ImageExport of the package pyqtgraph, and filed a bug report. As long as it is not fixed, there will be an issue.</source>
        <translation type="unfinished">Un bug est possible, le développeur a patché le fichier ImageExport du paquet pyqtgraph, et envoyé un rapport de bug. Tant que ce bug ne sera pas réparé, il y aura un problème.</translation>
    </message>
</context>
</TS>
