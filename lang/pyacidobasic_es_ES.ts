<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="es_ES" sourcelanguage="en_US">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="14"/>
        <source>Define a sample</source>
        <translation type="unfinished">Definicion de una muestra</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="45"/>
        <source>Sample: </source>
        <translation type="unfinished">Muestra: </translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="61"/>
        <source>Concentration (mol/L)</source>
        <translation type="unfinished">Concentración (mol/L)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="68"/>
        <source>0.1</source>
        <translation type="unfinished">0.1</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="75"/>
        <source>Volume (mL)</source>
        <translation type="unfinished">Volumen (mL)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="82"/>
        <source>10.0</source>
        <translation type="unfinished">10.0</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Forma</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="50"/>
        <source>Click to show/hide the plot</source>
        <translation type="unfinished">Clicar para mostrar/ocultar la curva</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="106"/>
        <source>Click to change the color of the plot</source>
        <translation type="unfinished">Clicar para cambiar el color de la curva</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../pyacidobasic/main.ui" line="74"/>
        <source>Laboratory</source>
        <translation type="unfinished">Laboratorio</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="104"/>
        <source>Choose reagents</source>
        <translation type="unfinished">Elección de los reactivos</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="189"/>
        <source>Burette</source>
        <translation type="unfinished">Bureta</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="728"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="249"/>
        <source>Beaker</source>
        <translation type="unfinished">Vaso de precipitados</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="788"/>
        <source>toolBar</source>
        <translation type="unfinished">Barra de herramientas</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="851"/>
        <source>pH</source>
        <translation type="unfinished">pH</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="26"/>
        <source>pyAcidoBasic</source>
        <translation type="unfinished">pyAcidoBasic</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="155"/>
        <source>Choose a reagent and drag it to the burette or the beaker</source>
        <translation type="unfinished">Elegir un reactivo y arrastrarlo hasta la bureta o el vaso de precipitados</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="158"/>
        <source>List of reagents: drag a reagent to the burette or the beaker.</source>
        <translation type="unfinished">Lista de reactivos: Arrastrar un reactivo hacia la bureta o el vaso de precipitados.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="201"/>
        <source>drain the burette to fill it again</source>
        <translation type="unfinished">vaciar la bureta para rellenarla de nuevo</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="230"/>
        <source>drag a reagent to the burette</source>
        <translation type="unfinished">arrastrar un reactivo hacia la bureta</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="233"/>
        <source>Reagent of the burette: drag a reagent from the above list.</source>
        <translation type="unfinished">Reactivo de la bureta: arrastrar un reactivo desde la lista de arriba.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="258"/>
        <source>drain the beaker to fill it again</source>
        <translation type="unfinished">vaciar el vaso de precipitados y rellenarlo de nuevo</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="287"/>
        <source>drag one or more reagents to the beaker</source>
        <translation type="unfinished">arrastrar uno o varios reactivos al vaso de precipitados</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="290"/>
        <source>List of reagents of the beaker: drag the reagents from the above list.</source>
        <translation type="unfinished">Lista de reactivos del vaso de precipitados: arrastrar reactivos desde la lista de arriba.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="643"/>
        <source>export in PDF format</source>
        <translation type="unfinished">exportar en formato PDF</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="660"/>
        <source>export in JPEG format</source>
        <translation type="unfinished">exportar en formato JPEG</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="677"/>
        <source>export in SVG format</source>
        <translation type="unfinished">exporter en formato SVG</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="694"/>
        <source>Define the title</source>
        <translation type="unfinished">Definir el título</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="708"/>
        <source>make the abscissa range wider</source>
        <translation type="unfinished">aumentar el rango de las abscisas</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="725"/>
        <source>make the abscissa range narrower</source>
        <translation type="unfinished">disminuir el rango de las abscisas</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="535"/>
        <source>Curves to plot</source>
        <translation type="unfinished">Curvas a trazar</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="761"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="771"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Ayuda</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="800"/>
        <source>&amp;Open ... (Ctrl-O)</source>
        <translation type="unfinished">&amp;Abrir... (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="808"/>
        <source>&amp;Save ... (Ctrl-S)</source>
        <translation type="unfinished">&amp;Guardar... (Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="816"/>
        <source>Save &amp;as ... (Shift Ctrl -S)</source>
        <translation type="unfinished">&amp;Guardar como... (Maj Ctrl -S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="821"/>
        <source>&amp;Quit (Ctrl-Q)</source>
        <translation type="unfinished">&amp;Salir (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="829"/>
        <source>&amp;About ...</source>
        <translation type="unfinished">&amp;Acerca de...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="834"/>
        <source>&amp;Manual (F1)</source>
        <translation type="unfinished">&amp;Manual (F1)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="859"/>
        <source>derivative</source>
        <translation type="unfinished">derivada</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="867"/>
        <source>[H3O+]</source>
        <translation type="unfinished">[H3O+]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="875"/>
        <source>[HO-]</source>
        <translation type="unfinished">[HO-]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="880"/>
        <source>Others... (Ctrl-T)</source>
        <translation type="unfinished">Otros... (Ctrl-T)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="888"/>
        <source>&amp;Examples ...</source>
        <translation type="unfinished">&amp;Ejemplos...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="125"/>
        <source>Filter:</source>
        <translation type="unfinished">Filtro:</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="113"/>
        <source>Type in a few charcters of the reagent</source>
        <translation type="unfinished">Teclear algunas letras del reactivo para seleccionarlo más fácilmente</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="138"/>
        <source>Click to delete the filter</source>
        <translation type="unfinished">Clicar para eliminar el filtro</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="515"/>
        <source>Concentrations/Quantities</source>
        <translation type="unfinished">Concentraciones/Cantidades</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="562"/>
        <source>&amp;Concentrations</source>
        <translation type="unfinished">&amp;Concentraciones</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="572"/>
        <source>&amp;Quantities</source>
        <translation type="unfinished">&amp;Cantidades</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="550"/>
        <source>Choose the type of plot</source>
        <translation type="unfinished">Elegir el tipo de representación</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="327"/>
        <source>Bromothymol blue</source>
        <translation type="unfinished">Azul de bromotimol</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="332"/>
        <source>Phenolphtalein</source>
        <translation type="unfinished">Fenolftaleína</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="337"/>
        <source>Helianthin (Methyl orange)</source>
        <translation type="unfinished">Naranja de metilo</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="342"/>
        <source>Bromophenol blue</source>
        <translation type="unfinished">Azul de bromofenol</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="347"/>
        <source>Methyl red</source>
        <translation type="unfinished">Rojo de metilo</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="322"/>
        <source>Color indicator ...</source>
        <translation type="unfinished">Indicador de pH...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="110"/>
        <source>Type in a few characters of the reagent</source>
        <translation type="unfinished">Teclear algunos caracteres del reactivo</translation>
    </message>
</context>
<context>
    <name>pyacidobasic</name>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="70"/>
        <source>Sorry, missing support</source>
        <translation type="unfinished">Lo sentimos, soporte incompleto</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="70"/>
        <source>The current version of pyqtgraph does not yet support PDF exports. Try to export SVG then convert it</source>
        <translation type="unfinished">La version actual de pyqtgraph no admite aún la exportación a PDF. Intente exportar en formato SVG e convierta el archivo posteriormente con un conversor externo</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="94"/>
        <source>Message about the bug</source>
        <translation type="unfinished">Mensaje sobre el error</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="94"/>
        <source>Error: %s</source>
        <translation type="unfinished">Error: %s</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="87"/>
        <source>A bug is possible</source>
        <translation type="unfinished">Un error es posible</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="270"/>
        <source>File to save</source>
        <translation type="unfinished">Archivo a guardar</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="270"/>
        <source>Acidobasic files [*.acb] (*.acb);; All files (*.* *)</source>
        <translation type="unfinished">Archivos Acidobasic [*.acb] (*.acb);; Todo tipo de archivos (*.* *)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="282"/>
        <source>Version error</source>
        <translation type="unfinished">Error de versión</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="282"/>
        <source>This file is not a valid Pyacidobasic file, version %s.</source>
        <translation type="unfinished">Este archivo, no es un archivo Pyacidobasic válido, de versión %s.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="294"/>
        <source>About</source>
        <translation type="unfinished">Acerca de</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="87"/>
        <source>A bug is possible, the developer has patched the file ImageExport of the package pyqtgraph, and filed a bug report. As long as it is not fixed, there will be an issue.</source>
        <translation type="unfinished">Un error puede producirse, el programador ha parcheado el archivo ImageExport del paquete pyqtgraph, y enviado una notificación del error. Mientras que éste no sea solucionado, el problema existirá.</translation>
    </message>
</context>
</TS>
