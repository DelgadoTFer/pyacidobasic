licence="""
    file reactifs.py: part of the package pyacidobasic version %s:

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt5.QtCore import *
from operator import attrgetter
import re

def setupUi(ui, listeAcidesBases, filtre=""):
    """
    met en place une liste de réactifs dans l'interface utilisateur
    de pyacidobasic
    @param ui, l'interface utilisateur de pyacidobasic
    @param listeAcidesBases une liste d'acides et de bases
    @param filtre une chaîne pour filtrer les acides et les bases à afficher
    """
    filtre=u"%s" %filtre # force QString -> unicode
    l=sorted(filter(lambda x: re.search(filtre,u"%s" %x.nom)!=None,
                    listeAcidesBases),
             key=attrgetter('nom'))
    ui.listWidgetReactifs.clear()
    for ab in l:
        ab.c=0.0 # on annule la concentration : elle est indéfinie
        ui.listWidgetReactifs.addItem(ab.nom)
        ui.listWidgetBecher.listeAcidesBases=listeAcidesBases
        ui.listWidgetBurette.listeAcidesBases=listeAcidesBases
